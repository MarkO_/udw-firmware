
#include "ILI9341.h"
#include "console9341.h"

int console_cursor_x = 0;
int console_cursor_y = 0;
static int console_fg = 0x1F1F;
static int console_bg = 0x0000;

void console_init()
{
	LCD_Init();
	console_cursor_x = 0;
	console_cursor_y = 0;
	console_fg = 0x1F1F;
	console_bg = 0x0000;
}

int console_raw_print(char *string)
{
	char *strp = string;
	char ch;
	int x;

	while (ch = (*strp++)) {
		if (ch == '\n') {
			/* erase to end of current line */
			console_erase_eol();
			console_cursor_x = 0;
			console_cursor_y++;
			if (console_cursor_y == CONSOLE_HEIGHT) {
				console_cursor_y--;
				console_vert_scroll(1);
			}
			continue;
		} else if (ch == '\r') {
			console_cursor_x = 0;
			continue;
		} else if (ch == '\b') {
			if (console_cursor_x > 0)
				console_cursor_x--;
			LCD_draw_char(console_cursor_x*CHARW,
				      console_cursor_y*CHARH,
				      ' ', console_fg, console_bg);
			continue;
		} else if (ch == '\f') {
			// console_cursor_x = 0;
			// console_cursor_y = 0;
			/* reset display and console fully */
			console_init();
			continue;
		} else if (ch < 32) {
			continue;
 		}
		
		LCD_draw_char(console_cursor_x*CHARW,
			      console_cursor_y*CHARH,
                              ch, console_fg, console_bg);
		console_cursor_x++;
		if (console_cursor_x == CONSOLE_WIDTH) {
			console_cursor_x = 0;
			console_cursor_y++;
			if (console_cursor_y == CONSOLE_HEIGHT) {
				console_cursor_y--;
				console_vert_scroll(1);
			}
		}
	}
	return strp - string;
}

int console_print(char * string)
{
	/* move more logic in here (erase_eol on NL etc) */
	return console_raw_print(string);
}

void console_print_char(char c)
{
	char buf[2];
	buf[0] = c;
	buf[1] = '\0';
	console_print(buf);
}

void console_vert_scroll(int lines)
{
	/* blank out top line and scroll around */
	// for now just wrap around to top of screen
	console_cursor_y = 0;
}

void console_erase_eol()
{
	int i;

	LCD_Address_Set(console_cursor_y * CHARH, console_cursor_x * CHARW,
			(console_cursor_y+1) * CHARH - 1, LCD_MAX_X - 1);
	for (i=0; i< (LCD_MAX_X - console_cursor_x*CHARW) * CHARH; i++) {
	      LCD_Write_DATA(console_bg >> 8);
	      LCD_Write_DATA(console_bg & 0xFF);
	}
}
