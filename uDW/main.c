/*
   DriveWire server implementation for uDW
   - uDW server entry point

   Copyright 2014-2015 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <misc.h>
#include <uart.h>
#include "udw.h"
#include "dw.h"
#include "vchan.h"

// # include "ILI9341.h"
// # include "console9341.h"
// void LCD_Color_Bars_Demo(void);

void Delay( uint32_t nTime );

__IO uint32_t Timer ;
__IO uint32_t soft_clock;
__IO int breathing_led;

int main(void)
{
	NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x5000);

	breathing_led = 0;
	disableJTAG();	/* to use PA14 as output */
	udw_setup_gpio();

	/* solid green LED while initializing */
	GPIO_WriteBit (LED_GPIO , LED_Pin , Bit_RESET );
	setRed(0);
	setGreen(255);

	// Configure SysTick Timer
	if ( SysTick_Config ( SystemCoreClock / 1000) )
		while (1);

	/* Set up pins for LCD */
	// LCD_GPIO_Config();
	/* Set up SPI for LCD */
	// spiInit(SPI1);

	/* will delay 100 ms */
	/* also inits SPI for SD card */
	powerUpSD();

	/* wait while client serial port stabilizes */
	Delay(100);
	Timer = 0xFFFF;

#ifdef UDWDEBUG
	uart_open(DEBUG_UART, 115200, 0);
#endif

	// debug_print("Starting LCD\r\n");
	// LCD_Init();
	// debug_print("Starting rectangles\r\n");
	// console_print("Starting rectangles\r\n");
	// LCD_Color_Bars_Demo();
	// debug_print("Done rectangles\r\n");

	dw_server(IO_UART);		/* will never return */
	return 0;
}

// Timer code
__IO uint32_t TimingDelay ;

void Delay( uint32_t nTime ) {
	TimingDelay = nTime ;
	while ( TimingDelay != 0);
}

void SysTick_Handler (void) {
	static int clock_ms = 1000;
	static int led_count;

	if ( TimingDelay != 0x00)
		TimingDelay --;
	Timer--;

	if (--clock_ms == 0) {
		clock_ms = 1000;
		soft_clock++;
	}
	if (breathing_led) {
		if (led_count++ == 2000) {
			led_count = 0;
		}
		if (led_count >= 256 + 512) {
			setGreen(0);
		} else if (led_count < 256) {
			setGreen(led_count);
		} else {
			setGreen((256 + 512 - 1 - led_count) >> 1);
		}
	} else {
		led_count = 256 + 512; /* start quiet */
	}
}

#ifdef USE_FULL_ASSERT
void assert_failed ( uint8_t * file , uint32_t line)
{
	/* Infinite loop */
	/* Use GDB to find out why we're here */
	while (1);
}
#endif
