/*
   DriveWire server implementation for uDW
   - uDW specific RTC and time functions

   Copyright 2014-2015 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stm32f10x_rcc.h>
#include <ff.h>
#include "rtc_32f1.h"
#include "ufuncs.h"

int rtc_running()
{
	return RCC_GetFlagStatus(RCC_FLAG_LSERDY);
}

void rtc_start_crystal()
{
	/* reset BKP */
}

void rtc_finish()
{
	/* store BKP flag */
}

int setup_time(int reset)
{
	return rtc_initialize(reset);
}

void setdate(const char *timestr)
{
		RTCTIME rtc;
		int tmp;
		int len;
		int century;

		rtc_gettime(&rtc);
		/* read century */
		len = uscanf2d(timestr, &century);
		if (!len)
			return;
		timestr += len;
		/* read year within century */
		len = uscanf2d(timestr, &tmp);
		if (!len)
			return;
		rtc.year = (uint8_t) tmp + century * 100;
		timestr += len;
		/* read month */
		len = uscanf2d(timestr, &tmp);
		if (!len)
			return;
		rtc.month = (uint8_t) tmp;
		timestr += len;
		/* read day of month */
		len = uscanf2d(timestr, &tmp);
		if (!len)
			return;
		rtc.mday = (uint8_t) tmp;
		rtc_settime(&rtc);
}

void settime(const char *timestr)
{
		RTCTIME rtc;
		int tmp;
		int len;

		rtc_gettime(&rtc);
		len = uscanf2d(timestr, &tmp);
		if (!len)
			return;
		rtc.hour = (uint8_t) tmp;
		timestr += len;
		len = uscanf2d(timestr, &tmp);
		if (!len)
			return;
		rtc.min = (uint8_t) tmp;
		timestr += len;
		/* set seconds if given */
		len = uscanf2d(timestr, &tmp);
		if (len)
			rtc.sec = (uint8_t) tmp;
		rtc_settime(&rtc);
}

/* for FatFs timestamps */
DWORD get_fattime(void)
{
	RTCTIME rtc;
	DWORD ftime;

	rtc_gettime(&rtc);
	ftime = ((rtc.year - 1980) & 0x7F) << 25;
	ftime |= rtc.month << 21;
	ftime |= rtc.mday << 16;
	ftime |= rtc.hour << 11;
	ftime |= rtc.min << 5;
	ftime |= rtc.sec >> 1;
	return ftime;
}
