/*
   DriveWire server implementation for uDW
   - hardware abstraction routines for uDW

   Copyright 2014 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* configuration for uDW */

/* DriveWire specification allows up to 15 */
#define VIRTCHANS 4

#define DEBUG_UART USART1
#define IO_UART USART3
#define WIFI_UART USART2

#define SD_SPI SPI2

/* Maple Mini LED is PB1 */
#define LED_GPIO GPIOB
#define LED_Pin GPIO_Pin_1
#define LED_RCC_APB2Periph RCC_APB2Periph_GPIOB

/* uDW: red LED is PB6, green LED is PB7 */
#define RED_GPIO GPIOB
#define RED_Pin GPIO_Pin_6
#define RED_RCC_APB2Periph RCC_APB2Periph_GPIOB
#define GREEN_GPIO GPIOB
#define GREEN_Pin GPIO_Pin_7
#define GREEN_RCC_APB2Periph RCC_APB2Periph_GPIOB

/* uDW: SD card power control is PA14 */
#define SD_PWR_GPIO GPIOA
#define SD_PWR_Pin GPIO_Pin_14
#define SD_PWR_APB2Periph RCC_APB2Periph_GPIOA

/* uDW: SD card NSS is PB12 */
#define SD_NSS_GPIO GPIOB
#define SD_NSS_Pin GPIO_Pin_12
#define SD_NSS_APB2Periph RCC_APB2Periph_GPIOB

/* uDW: read SD card detect on PA15 */
#define SCD_GPIO GPIOA
#define SCD_Pin GPIO_Pin_15
#define SCD_RCC_APB2Periph RCC_APB2Periph_GPIOA

/* uDW: sound/tape out on PA8 */
#define SND_GPIO GPIOA
#define SND_Pin GPIO_Pin_8
#define SND_RCC_APB2Periph RCC_APB2Periph_GPIOA

/* uDW: wifi power down on PB4 */
#define WFPD_GPIO GPIOB
#define WFPD_Pin GPIO_Pin_4
#define WFPD_RCC_APB2Periph RCC_APB2Periph_GPIOB

void udwResetToDFU(void);
int udw_setup_gpio(void);
int udwCardDetect(void);
void setRed(int intensity);
void setGreen(int intensity);

void powerSD(int enable);
void powerUpSD(void);
void powerDownSD(void);
void disableJTAG(void);
void snd_out(int value);
void snd_test(int duration);
void hf_test(int duration);
void powerWIFI(int enable);
void enable_wifi_irq(void);

/* for debug build */
#ifndef UDWDEBUG
# define debug_print(...)
# define debug_print_char(...)
# define debug_print_2x(...)
# define debug_print_4x(...)
# define debug_print_value(...)
# define DEBUGVER
#else
# define debug_print(...) print_uart(DEBUG_UART, __VA_ARGS__)
# define debug_print_char(...) uart_putc(__VA_ARGS__, DEBUG_UART)
# define DEBUGVER "-DEBUG"
#endif

/* cludge to stringify version macro */
#if defined UDWVERSION
#define xstr(s) str(s)
#define str(s) #s
#define UDWVERSION_STRING " VERSION " xstr(UDWVERSION) DEBUGVER
#else
#define UDWVERSION_STRING " DEVELOPMENT VERSION" DEBUGVER
#endif
